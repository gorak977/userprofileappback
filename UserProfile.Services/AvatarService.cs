﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserProfileApp.Core.Abstractions;
using UserProfileApp.Core.Abstractions.Services;
using UserProfileApp.Core.DTO;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.Services
{
    public class AvatarService : IAvatarService
    {
        private IUnitOfWork UnitOfWork;
        private IMapper _mapper;

        public AvatarService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            UnitOfWork = unitOfWork;
        }
        public List<AvatarDTO> GetAll()
        {
            return UnitOfWork.AvatarRepository.GetAll()
                .Select(item => _mapper.Map(item, new AvatarDTO())).ToList();
        }

        public AvatarDTO Get(int id)
        {
            var user = UnitOfWork.AvatarRepository.Get(id);
            return _mapper.Map(user, new AvatarDTO());
        }

        public AvatarDTO Add(AvatarDTO avatarDTO)
        {
            Avatar avatar = UnitOfWork.AvatarRepository.Add(_mapper.Map(avatarDTO, new Avatar()));
            return _mapper.Map(avatar, new AvatarDTO());
        }

        public void Remove(int id)
        {
            UnitOfWork.AvatarRepository.Remove(id);
        }

        public AvatarDTO Update(AvatarDTO avatarDTO)
        {
            UnitOfWork.AvatarRepository.Update(_mapper.Map(avatarDTO, new Avatar()));
            return _mapper.Map(UnitOfWork.AvatarRepository.Get(avatarDTO.ID), new AvatarDTO());
        }
    }
}
