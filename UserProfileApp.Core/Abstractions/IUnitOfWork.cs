﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.Abstractions.Repositories;

namespace UserProfileApp.Core.Abstractions
{
    public interface IUnitOfWork : System.IDisposable
    {
        IUserRepository UserRepository { get; }
        IRoleRepository RoleRepository { get; }
        IAvatarRepository AvatarRepository { get; }

    }
}
