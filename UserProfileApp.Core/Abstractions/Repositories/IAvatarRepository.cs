﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.Core.Abstractions.Repositories
{
    public interface IAvatarRepository:IRepository<Avatar>
    {

    }
}
