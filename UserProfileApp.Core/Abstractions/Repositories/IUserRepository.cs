﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserProfileApp.Core.DTO;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.Core.Abstractions.Repositories
{
    public interface IUserRepository:IRepository<User>
    {
    }
}
