﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.DTO;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.Core.Abstractions.Services
{
    public interface IAvatarService : IService<AvatarDTO>
    {
    }
}
