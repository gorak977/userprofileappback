﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.DTO;

namespace UserProfileApp.Core.Abstractions.Services
{
    public interface IRoleService : IService<RoleDTO>
    {
    }
}
