﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserProfileApp.Core.Abstractions.Services
{
    public interface IService<TEntity> where TEntity : class
    {
        public List<TEntity> GetAll();

        public TEntity Get(int id);

        public TEntity Add(TEntity entity);

        public TEntity Update(TEntity entity);

        public void Remove(int id);
    }
}
