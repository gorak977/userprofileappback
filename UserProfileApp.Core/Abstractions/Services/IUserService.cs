﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.DTO;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.Core.Abstractions.Services
{
    public interface IUserService : IService<UserFullInfoDTO>
    {
        public List<UserShortInfoDTO> GetAllShortInfo();

    }
}
