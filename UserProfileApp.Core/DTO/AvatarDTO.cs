﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace UserProfileApp.Core.DTO
{
    public class AvatarDTO
    {
        public int ID { get; set; }
        public byte[] Image { get; set; }
    }
}
