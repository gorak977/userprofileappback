﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserProfileApp.Core.DTO
{
    public class RoleDTO
    {
        public int ID { get; set; }
        public string RoleName { get; set; }
    }
}
