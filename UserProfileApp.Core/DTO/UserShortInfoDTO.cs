﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.Core.DTO
{
    public class UserShortInfoDTO
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public int RoleID { get; set; }

        public string Role { get; set; }
    }
}
