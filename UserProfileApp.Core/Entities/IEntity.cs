﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserProfileApp.Core.Entities
{
    public interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
