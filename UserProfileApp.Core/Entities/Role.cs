﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UserProfileApp.Core.Entities
{
    public class Role : IEntity<int>
    {
        [Key]
        public int ID { get; set; }
        public List<User> Users { get; set; }
        
        [Required]
        public string RoleName { get; set; }
    }
}
