﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace UserProfileApp.Core.Entities
{
    public class User : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required, MaxLength(50)]
        public string FirstName { get; set; }

        [Required, MaxLength(50)]
        public string LastName { get; set; }
#nullable enable
        public string? MiddleName { get; set; }
        public DateTime BirthDate { get; set; }

        [MaxLength(1024)]
#nullable enable
        public string? Description { get; set; }
      
        [ForeignKey("Avatar")]
        public int? AvatarID { get; set; }
#nullable enable
        public Avatar? Avatar { get; set; }

        [ForeignKey("Role")]
        public int RoleID { get; set; }
#nullable disable
        public Role Role { get; set; }

        public static int GetAge(DateTime dateOfBirth)
        {
            var today = DateTime.Today;

            var a = (today.Year * 100 + today.Month) * 100 + today.Day;
            var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

            return (a - b) / 10000;
        }
    }
}
