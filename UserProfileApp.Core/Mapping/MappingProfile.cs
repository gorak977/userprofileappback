﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UserProfileApp.Core.DTO;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.Core.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserShortInfoDTO>()
                .ForMember(shortInfo => shortInfo.FullName, fullInfo => fullInfo
                    .MapFrom(item => item.FirstName + " " + item.LastName + " " + item.MiddleName))
                .ForMember(shortInfo => shortInfo.Age, fullInfo => fullInfo
                    .MapFrom(item => User.GetAge(item.BirthDate)))
                .ForMember(shortInfo => shortInfo.Role, fullInfo => fullInfo
                    .MapFrom(item => item.Role.RoleName));

            CreateMap<User, UserFullInfoDTO>().ReverseMap();

            CreateMap<Role, RoleDTO>().ReverseMap();

            CreateMap<Avatar, AvatarDTO>().ReverseMap();

        }


    }


}
