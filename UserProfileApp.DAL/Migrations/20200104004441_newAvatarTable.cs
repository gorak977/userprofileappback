﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UserProfileApp.DAL.Migrations
{
    public partial class newAvatarTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "AvatarID",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Avatars",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Image = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avatars", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_AvatarID",
                table: "Users",
                column: "AvatarID");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Avatars_AvatarID",
                table: "Users",
                column: "AvatarID",
                principalTable: "Avatars",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Avatars_AvatarID",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Avatars");

            migrationBuilder.DropIndex(
                name: "IX_Users_AvatarID",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "AvatarID",
                table: "Users");

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "Users",
                type: "varbinary(max)",
                nullable: true);
        }
    }
}
