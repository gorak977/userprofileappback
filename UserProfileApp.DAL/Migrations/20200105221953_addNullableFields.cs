﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UserProfileApp.DAL.Migrations
{
    public partial class addNullableFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Avatars_AvatarID",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "AvatarID",
                table: "Users",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Avatars_AvatarID",
                table: "Users",
                column: "AvatarID",
                principalTable: "Avatars",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Avatars_AvatarID",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "AvatarID",
                table: "Users",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Avatars_AvatarID",
                table: "Users",
                column: "AvatarID",
                principalTable: "Avatars",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
