﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.Abstractions.Repositories;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.DAL.Repositories
{
    class AvatarRepository: Repository<Avatar> ,IAvatarRepository
    {
        public AvatarRepository(UserProfileAppContext _context) : base(_context)
        {
        }

    }
}
