﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.Abstractions.Repositories;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.DAL.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(UserProfileAppContext _context) : base(_context)
        {
        }
    }
}
