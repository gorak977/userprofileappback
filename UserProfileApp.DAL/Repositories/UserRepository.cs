﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UserProfileApp.Core.Abstractions.Repositories;
using UserProfileApp.Core.DTO;
using UserProfileApp.Core.Entities;

namespace UserProfileApp.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(UserProfileAppContext _context) : base(_context)
        {
        }
    }
}
