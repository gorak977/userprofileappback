﻿using System;
using System.Collections.Generic;
using System.Text;
using UserProfileApp.Core.Abstractions;
using UserProfileApp.Core.Abstractions.Repositories;
using UserProfileApp.DAL.Repositories;

namespace UserProfileApp.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private IUserRepository userRepository;
        private IRoleRepository roleRepository;
        private IAvatarRepository avatarRepository;


        private UserProfileAppContext _context;
        public UnitOfWork(UserProfileAppContext context)
        {
            _context = context;
        }
        public IUserRepository UserRepository
        {
            get
            {
                return userRepository ??= new UserRepository(_context);
            }
        }
        public IRoleRepository RoleRepository
        {
            get
            {
                return roleRepository ??= new RoleRepository(_context);
            }
        }
        public IAvatarRepository AvatarRepository
        {
            get
            {
                return avatarRepository ??= new AvatarRepository(_context);
            }
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
